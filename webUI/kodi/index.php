<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
<!--
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <?php include("fav.php");?>
<style>
  #main, p{
    margin-top: 20px;
  }

.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}
</style>
<script>
//Good Reference
//http://habitech.s3.amazonaws.com/PDFs/STR/STREAM_Box%20Note%20-%20JSON%20API%20Examples%20for%20ContentPlayer%20(Isengard).pdf

var ip="192.168.1.146";
var irip="192.168.1.12";
var port="80";


$(document).ready(function(){
  $(".ctrl").click(playPause);

  $(".navi").click(nav);

  $(".tv").click(TV);

  //volume slider
  var slider = document.getElementById("myRange");
  slider.oninput = function() {
    volume(this.value);
  }
});

function TV(){
  var d = $(this).attr('cmd');
  var url="http://"+irip+"/ir?code="+d;
  $.get(url);
}

function volume(v){
  $.ajax({
  type: 'GET',
    url: 'http://' + ip + ":" + port + '/jsonrpc',
    dataType: 'jsonp',
    //    jsonpCallback: 'jsonCallback',
    type: 'GET',
    async: true,
    timeout: 5000,
    data: 'request=' + encodeURIComponent( '{"jsonrpc": "2.0", "method": "Application.SetVolume", "params": {"volume": '+v+'}, "id": 1}' )
});

}

function playPause(){
  var d = $(this).attr('cmd');
  $.ajax({
  type: 'GET',
    url: 'http://' + ip + ":" + port + '/jsonrpc',
    dataType: 'jsonp',
    jsonpCallback: 'jsonCallback',
    type: 'GET',
    async: true,
    timeout: 5000,
    data: 'request=' + encodeURIComponent( '{"jsonrpc": "2.0", "method": "Player.'+d+'", "params": { "playerid": 1 }, "id": 1}' )
  });
}

function nav(){
  var d = $(this).attr('cmd');
  $.ajax({
  type: 'GET',
    url: 'http://' + ip + ":" + port + '/jsonrpc',
    dataType: 'jsonp',
    jsonpCallback: 'jsonCallback',
    type: 'GET',
    async: true,
    timeout: 5000,
    data: 'request=' + encodeURIComponent( '{"jsonrpc": "2.0", "method": "Input.' + d + '", "id": 1}' )
  });


}
</script>
  </head>
  <body>

    <div id="main" class="container">
      <div class="row">
<div class="btn-group btn-group-justified">
  <a href="#" class="btn btn-primary btn-danger tv" cmd="E0E040BF">POWER </a>
  <a href="#" class="btn btn-primary tv" cmd="E0E0807F">SOURCE</a>
  <a href="#" class="btn btn-primary tv" cmd="E0E058A7">MENU</a>
  <a href="#" class="btn btn-primary tv" cmd="E0E0D02F">Vol-</a>
  <a href="#" class="btn btn-primary tv" cmd="E0E0E01F">Vol+</a>
</div>
<br>
        <button type="button" cmd="PlayPause" class="btn btn-primary btn-block ctrl">Play/Pause</button>
        <button type="button" cmd="Stop" class="btn btn-primary btn-block ctrl">Stop</button>
        <p>Volume:</p>
        <input type="range" min="1" max="100" value="50" class="slider" id="myRange">         

        <br><br>
        <button type="button" class="btn btn-primary btn-block navi" cmd="up">UP</button>
        <button type="button" class="btn btn-primary btn-block navi" cmd="down">DOWN</button>
        <button type="button" class="btn btn-primary btn-block navi" cmd="select">SELECT</button>
        <br>
<div class="btn-group btn-group-justified">
  <a href="#" class="btn btn-primary navi" cmd="left"> << </a>
  <a href="#" class="btn btn-primary navi" cmd="right"> >> </a>
</div>
        <br>

        <button type="button" class="btn btn-primary btn-block navi" cmd="ShowOSD">ShowOSD</button>
      </div>

    </div>
  </body>
</html>

